var Hapi = require( "hapi" );
var fs = require( "fs" );
var inert = require( "inert" );

var server = new Hapi.Server();

server.connection({
    port : process.env.PORT || 8080,
    host : '0.0.0.0'
});

server.route({
    method : 'POST',
    path : '/',
    handler : function ( request, reply ) {
        var raw = request.payload.data.replace( /^data:image\/jpeg;base64,/, "" );

        fs.writeFile( './picture.jpeg', raw, 'base64', function ( err ) {
            if ( err ) {
                console.log( 'failed to write image' );
            }
        } );

        return reply( 'Hello world' );
    }
});

server.register( require( "inert" ), function ( err ) {
    if ( err ) {
        throw err;
    }

    server.route({
        method : 'GET',
        path : '/picture',
        handler : function ( request, reply ) {
            reply.file( 'picture.jpeg' );
        }
    })
} )

server.start( function ( err ) {
    if ( err ) {
        throw err;
    }

    console.log( 'server starting' );
} )
