var NodeWebcam = require('node-webcam');
var querystring = require('querystring');
var http = require('http');

var Webcam = NodeWebcam.create({
    callbackReturn : 'base64',
    saveShots : false,
    delay : 0,
    output : 'jpeg'
});

function submit ( data ) {
    var query = querystring.stringify( {
        'data' : data
    } );

    var post_options = {
        host : 'the-shattle-capture.herokuapp.com',
        port : '80',
        path : '/',
        method : 'POST',
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(query)
        }
    };

    var post_request = http.request( post_options, function ( res, err ) {
        res.setEncoding( 'utf8' );

        res.on( 'end', function (){
            console.log( 'done' );
        } );

        res.on( 'data', function ( chunk ) {
            console.log( 'response' );
        } );
    } );

    post_request.on( 'error', function ( error ) {
        console.log( error );
    } );

    post_request.write( query );
    post_request.end();
}

function capture () {
    console.log( 'capturing' );

    Webcam.capture( 'picture', function ( err, data ) {
        if ( err ) {
            throw err;
        }

        submit( data );
    } );

    setTimeout( capture, 2500 );
}

capture();
